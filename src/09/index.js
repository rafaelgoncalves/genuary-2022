
const s09 = (p) => {
  const windowWidth = 800;
  const windowHeight = 800;
  const n = 10;
  const colorRate = 0.33;
  const defaultSize = 0.5;
  const colors = [
    "#FF2020",
    "#20FF20",
    "#2020FF",
    "#FFFFFF",
    "#FF8020",
    "#FFFF20",
    "#8020FF",
    "#202020",
  ]
  let cubes = [];

  p.setup = function () {
    p.createCanvas(windowWidth, windowHeight, p.WEBGL);
    const getColor = () => colors[Math.floor(p.random(colors.length))];
    for (let i=0; i<n; i++){
      cubes.push({
        color: getColor(),
        x: defaultSize*p.width*p.random(),
        y: defaultSize*p.height*p.random(),
        z: defaultSize*Math.max(p.width, p.height)*p.random(),
        translateX: defaultSize*p.width*(p.random() - 0.5),
        translateY: defaultSize*p.height*(p.random() - 0.5),
        translateZ: defaultSize*Math.min(p.width,p.height)*(p.random() - 0.5),
      });
    };
  };
  p.draw = function () {
    p.orbitControl();
    p.background(10,10,10);

    for (let i=0; i<n; i++){
      p.push();
      p.fill(cubes[i].color);
      p.translate(
          cubes[i].translateX,
          cubes[i].translateY,
          cubes[i].translateZ,
          );
      p.box(
          cubes[i].x,
          cubes[i].y,
          cubes[i].z,
          );
      p.pop();
    };
    if (p.keyIsDown(p.LEFT_ARROW)) {
      p.save();
    }
  };
};

const sk09 = {
  title: "Colored blocks",
  description: "Colorful 3D boxes",
  canvas: s09,
};
