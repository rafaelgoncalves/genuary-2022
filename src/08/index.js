
const s08 = (p) => {
  const windowWidth = 500;
  const windowHeight = 500;
  const A = 200;
  const B = 200;
  let a = 12;
  const b = 10;
  const drawStep = 0.005;
  const phaseStep = 0.003;
  const aStep = 0.01;
  let phase=0;
  const opacity=0.7;
  const strokeWeight = 2;
  let startMillis;
  const fps = 30;
  const capture = false;
//  let capturer = new CCapture({ format: 'png', framerate: fps });
 // capturer.capture(document.getElementById('defaultCanvas0'));

  p.setup = function () {
    p.createCanvas(windowWidth, windowHeight);
    p.background(10,10,10);
    p.stroke('#FFFFFF');
    p.noFill();
    p.strokeWeight(strokeWeight);
    p.frameRate(fps);
  };

  p.draw = function () {
    p.background(10,10,10,opacity*255);
    p.beginShape();
    for (let step=0; step<2*p.PI; step+=drawStep) {
      let x = A*Math.sin(a*step + phase);
      let y = B*Math.sin(b*step);
      p.vertex(p.width/2 + x, p.height/2 + y);
    };
    p.endShape();
    phase += phaseStep;
    a += aStep;

    // Ccapture
    if (capture) {
      if (p.frameCount === 1) {
        capturer.start();
      };

      if (startMillis == null) {
        startMillis = p.millis();
      }

     let duration = 45000;
     let elapsed = p.millis() - startMillis;
     let t = p.map(elapsed, 0, duration, 0, 1);

    if (t > 1) {
        p.noLoop();
        console.log('finished recording.');
        capturer.stop();
        capturer.save();
        return;
      }
    capturer.capture(document.getElementById('defaultCanvas0'));
    };
  };

};

const sk08 = {
  title: "Lissajous",
  description: "Continously changing phase and frequency of 2 sinousoidal waves and plotting x and y.",
  canvas: s08,
};
