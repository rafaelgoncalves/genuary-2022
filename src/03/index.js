// Space

const s03 = (p) => {
  const windowWidth = 600;
  const windowHeight = 600;
  const BG = 255;

// Standard Normal variate using Box-Muller transform.
// inspired by: https://stackoverflow.com/questions/25582882/javascript-math-random-normal-distribution-gaussian-bell-curve
    const randn_bm = (mu, sigma) => {
    let u = 0, v = 0;
	    let r;
    while(u === 0) u = Math.random(); //Converting [0,1) to (0,1)
    while(v === 0) v = Math.random();
    return sigma*Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v )+mu;
                };


  p.setup = function () {
    p.createCanvas(windowWidth, windowHeight);
    p.noStroke();
    p.colorMode(p.RGB, 1);
    p.background(randn_bm(0, .1), randn_bm(0,.2), randn_bm(0,.5));
    r = randn_bm(0,1);	  
    p.fill(Math.random() > 0.5 ? 0 : 1);
    p.ellipse(20+randn_bm(-10,10), 30+randn_bm(-10,10), windowWidth*1.4*r, windowWidth*1.4*r); 
    p.fill(Math.random() > 0.3 ? "#FF081E" : Math.random() > 0.3 ? "#081EFF" : 0);
    p.ellipse(windowWidth*0.75*(1-r), windowHeight*0.8*(1-r), windowWidth*0.5, windowWidth*0.5); 
  };
};

const sk03 = {
  title: "Space",
  description: "Generative script inspired in the word 'space'. 'Space' was thought both in the sense of sideral space as well as in geometric and in color space."
,
  canvas: s03,
};
