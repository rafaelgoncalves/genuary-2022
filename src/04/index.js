// New new fidenza
// Inspired by: https://tylerxhobbs.com/fidenza

const s04 = (p) => {
  const windowWidth = 600;
  const windowHeight = 600;
  const resScale = 0.01;
  const sizeScale = 1.5;
  //const turbulence = 0.005;
  const turbulence = 0.02;
  const BG = 255;
  const strokeCapValue = "square";
  const margin = null;
  const dontCollide = false;

  p.setup = function () {
    p.createCanvas(windowWidth, windowHeight);
    const resolution = Math.floor(resScale * Math.max(p.height, p.width));
    const leftX = Math.floor(p.width * (1 - sizeScale));
    const rightX = Math.floor(p.width * sizeScale);
    const topY = Math.floor(p.width * (1 - sizeScale));
    const bottomY = Math.floor(p.width * sizeScale);
    const numColumns = (rightX - leftX) / resolution;
    const numRows = (bottomY - topY) / resolution;

    // Create grid of flow field
    let grid = new Array(numColumns).fill(new Array(numRows)); ;
    for (let i=0; i < numRows; i++) {
      for (let j=0; j < numColumns; j++) {
        let scaledX = i * turbulence;
        let scaledY = j * turbulence;
        let noiseVal = p.noise(scaledX, scaledY);
        let angle = p.map(noiseVal, 0.0, 1.0, 0.0, p.PI*2.0);
        grid[j][i] = angle;
      };
    };

    // Based on https://p5js.org/reference/#/p5.Vector/magSq
    const drawArrow = (base, vec, myColor = 0) => {
      p.push();
      p.stroke(myColor);
      p.strokeWeight(2);
      p.fill(myColor);
      p.translate(base.x, base.y);
      p.line(0, 0, vec.x, vec.y);
      p.rotate(vec.heading());
      let arrowSize = 3;
      p.translate(vec.mag() - arrowSize, 0);
      p.triangle(0, arrowSize / 2, 0, -arrowSize / 2, arrowSize, 0);
      p.pop();
    }

    p.strokeCap(strokeCapValue === "square" ? p.SQUARE : strokeCapValue === "project" ? p.PROJECT : p.ROUND);
    p.push();
    p.fill(p.color(BG));
    p.rect(-1, -1, p.width+1, p.height+1);
    p.pop();
    
    p.background(p.color(BG));

// // Draw grid for DEBUG
//    for (let i=0; i < numRows; i++) {
//      for (let j=0; j < numColumns; j++) {
//        p.push();
//        p.translate(leftX + j*resolution, topY + i*resolution);
//        p.fill(0);
//        drawArrow(p.createVector(0,0), p.createVector(10*Math.cos(grid[j][i]),10*Math.sin(grid[j][i])));
//        p.pop();
//      };
//    };

    let d = p.pixelDensity();
    for (let m = 0; m < 1000; m++) {
      p.loadPixels();
      let x=Math.random()*p.width, y=Math.random()*p.height;
      let stepLen = Math.random() < 0.5 ? 1 : -1;
      p.stroke(Math.random()*255, Math.random()*255, Math.random()*255);
      p.noFill();
      let wgt = Math.floor(100*Math.random());
      let marg = 0.1;
      p.strokeWeight(wgt);
      p.beginShape();
      for (let n = 0; n < 500; n++) {

       let insideCanvas = (x < p.width && x >= 0 && y < p.height && y >= 0);
       if (dontCollide && insideCanvas){
         let pos = Math.floor(4*d*(y*p.width + x));
         let pos1 = Math.floor(4*d*(y*p.width + x+marg*wgt));
         let pos2 = Math.floor(4*d*(y*p.width + x-marg*wgt));
         let pos3 = Math.floor(4*d*((y+marg*wgt)*p.width + x));
         let pos4 = Math.floor(4*d*((y-marg*wgt)*p.width + x));
         let [c1, c2, c3, c4] = [pos1, pos2, pos3, pos4].map(
           (e) => p.color(
           p.pixels[e],
           p.pixels[e+1],
           p.pixels[e+2],
           p.pixels[e+3]
         ));
         //console.log(c.toString() === p.color(BG).toString());
         if ([c1, c2, c3, c4].reduce((ret, c) => c.toString() != p.color(BG).toString() || ret, false))
           break;
       };

        p.curveVertex(x, y);
      
        let xOffset = x - leftX;
        let yOffset = y - topY;
      
        let columnIndex = Math.floor(xOffset / resolution);
        let rowIndex = Math.floor(yOffset / resolution);
      
        // NOTE: normally you want to check the bounds here
        if (0 <= columnIndex && columnIndex < numColumns && 0 <= rowIndex && rowIndex < numRows) {
          let gridAngle = grid[columnIndex][rowIndex];
      
          xStep = stepLen * Math.cos(gridAngle);
          yStep = stepLen * Math.sin(gridAngle);
      
          x = x + xStep;
          y = y + yStep;
        };
      };
      p.endShape();
    }; 
    
  };
};

const sk04 = {
  title: "New new fidenza",
  description: 'Generative script inspired by <a href="https://tylerxhobbs.com/fidenza">Tyler Hobbs Fidenza</a>.',
  canvas: s04,
};
