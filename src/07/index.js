
const s07 = (p) => {
  const windowWidth = 600;
  const windowHeight = 1000;
  const rows = 15;
  // attenuator
  const att = 0.3;
  const colors = [
    "#e32636",
    "#ffbf00",
    "#32cd32",
    "#007fff",
    "#855c91",
    "#e9692c",
  ];
  const size = 100;
  let width;
  let height;

  p.setup = function () {
    p.createCanvas(windowWidth, windowHeight);
    const vGap = p.height/rows;
    height = -1*vGap;
    p.noStroke();
    do {
    height += vGap;
    width = -2*size;
      do {
          p.push();
          let r = Math.floor(p.random(colors.length));
          let color = colors[r];
          let step = size*(Math.random()-0.5);
          p.fill(color);
          p.translate(width, height);
          p.beginShape();
          p.vertex(size*att*(Math.random()-0.5), 0);
          p.vertex(size*att*(Math.random()-0.5), vGap);
          p.vertex(2*size+step, vGap);
          p.vertex(2*size+step, 0);
          p.endShape(p.CLOSE);
          p.pop();
          width += size+step;;
      } while (width < p.width);
    } while (height < p.height);
  };

};

const sk07 = {
  title: "Colored blocks",
  description: "Inspired in Sol LeWitt wall drawings.",
  canvas: s07,
};
