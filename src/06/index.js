
const s06 = (p) => {
  const windowWidth = 600;
  const windowHeight = 600;
  const n = 2;
  const opacity = 0.6;
  const step = 0.006;
  const strokeWeight =4;
  let points = [];
  let idx;

  p.setup = function () {
    p.createCanvas(windowWidth, windowHeight);
    p.background('#202020');
    p.strokeWeight(strokeWeight);
    for (let i=0; i<n; i++) {
      points.push({
      x: 100*Math.random(),
      y: 100*Math.random(),
      color: p.color(Math.random()*255, Math.random()*255, Math.random()*255, opacity*100),
    });
    };
  };

  p.draw = function () {
    points.forEach(({x, y, color}, idx) => {
    p.push();
    p.stroke(color);
    p.point(p.noise(x)*p.width, p.noise(y)*p.height);
    p.pop();
      
    x += step;
    y += step;

    points[idx]={x, y, color};
    });
  };
};

const sk06 = {
  title: "Random walks",
  description: 'Simple experiment with different actualization rules.',
  canvas: s06,
};
