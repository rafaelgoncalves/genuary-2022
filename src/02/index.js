// Dithering
// inspired by https://surma.dev/things/ditherpunk/

const s02 = (p) => {
  const inputWidth = 387;
  const inputHeight = 581;
  const imgWidth = Math.floor(inputWidth);
  const imgHeight = Math.floor(inputHeight);
  const windowWidth = Math.floor(2*inputWidth);
  const windowHeight = Math.floor(2*inputHeight);
  const BG = 255;
  const FG = 8;
  let img;
  let c;
  let b;
	
  p.preload = function () {
    img = p.loadImage('02/input.png');
    bluenoise = p.loadImage('02/HDR_L_0.png');
  };

  p.setup = function () {
    p.createCanvas(windowWidth, windowHeight);
    img.resize(imgWidth,imgHeight);
     p.background(BG);
     p.fill(FG);
     p.noStroke();
     img.loadPixels();
     bluenoise.loadPixels();
     p.image(img, 0, 0);
     const getBlueNoiseAt = (j) => {
           let _c = [
          	 bluenoise.pixels[(j) %   (64*64)],
          	 bluenoise.pixels[(j+1) % (64*64)],
          	 bluenoise.pixels[(j+2) % (64*64)],
          	 bluenoise.pixels[(j+3) % (64*64)],
           ];
           return p.map(p.brightness(_c), 0, 100, 0, 1);
     };
     const d = p.pixelDensity();
     const dithering = [{label: "threshold", x: 1, y: 0, algo: (b) => b < 0.5 ? 0 : 255}, {label: "white", x: 0, y: 1, algo: b => b + Math.random() - 0.5 > 0.5 ? 255 : 0}, {label: "blue", x: 1, y: 1, algo: (b, j) => b + getBlueNoiseAt(j) - 0.5 > 0.5 ? 255 : 0}];
     for (let i = 0; i < 3; i++) {
       let output = p.createImage(imgWidth, imgHeight);
       output.loadPixels();
       for (let j=0; j < 4*d*imgWidth*imgHeight ; j = j + 4) {
           c = [
          	 img.pixels[j],
          	 img.pixels[j+1],
          	 img.pixels[j+2],
          	 img.pixels[j+3],
           ];
           b = p.map(p.brightness(c), 0, 100, 0, 1);
	   out = p.color(dithering[i].algo(b, j));
	   output.pixels[j] = p.red(out);
	   output.pixels[j + 1] = p.green(out);
	   output.pixels[j + 2] = p.blue(out);
	   output.pixels[j + 3] = p.alpha(out);
         };
	     
       output.updatePixels();
       p.image(output, dithering[i].x*imgWidth, dithering[i].y*imgHeight);

       p.push();
       p.translate((dithering[i].x + 1)*imgWidth - 100, (dithering[i].y+1)*imgHeight - 20);
       p.rect(0, 0, 100, 20);
       p.fill(BG);
       p.text(dithering[i].label, 10, 15);
       p.pop();
    };
  };
};

const sk02 = {
  title: "Dithering",
  description: "Dithering algorithms inspired by <a href='https://surma.dev/things/ditherpunk/'>Surma</a>. 64x64 precomputed blue noise array is from <a href='http://momentsingraphics.de/BlueNoise.html'>here</a>."
,
  canvas: s02,
};
