// Portrait in 10000 squares
// inspired by https://timrodenbroeker.de/how-to-rasterize-an-image-with-processing/

const s01 = (p) => {
  const n = 10000;
  const windowWidth = 675;
  const windowHeight = 900;
  const BG = 255;
  const FG = 8;
  let img;
  let c;
  let b;
	
  p.preload = function () {
    img = p.loadImage('01/self.jpeg');
  };

  p.setup = function () {
     p.createCanvas(windowWidth, windowHeight);
     p.background(BG);
     p.fill(FG);
     p.noStroke();
     const ratio = p.height/p.width;
     const aux = Math.sqrt(n/ratio);
     const tilesX = Math.floor(aux);
     const tilesY = Math.floor(aux*ratio);
     const tileSize = p.width / tilesX;
     img.loadPixels();
     const d = p.pixelDensity();
     for (let y = 0; y < img.height; y = Math.floor(y + tileSize)) {
       for (let x = 0; x < img.width; x = Math.floor(x + tileSize)) {
         c = [
		 img.pixels[4*d*(y*img.width + x)],
		 img.pixels[4*d*(y*img.width + x)+1],
		 img.pixels[4*d*(y*img.width + x)+2],
		 img.pixels[4*d*(y*img.width + x)+3],
	 ];
         b = p.map(p.brightness(c), 0, 100, 1, 0);
         p.push();
         p.translate(x, y);
         p.rect(0, 0, b * tileSize, b * tileSize);
         p.pop();
       };
     };
  };
};

const sk01 = {
  title: "Portrait in 10,000 squares",
  description: "Self portrait rasterized from photography. Inspired by <a href='https://timrodenbroeker.de/how-to-rasterize-an-image-with-processing/'>Tim Rodenbröker</a>."
,
  canvas: s01,
};
