// Destroy a box

const s05 = (p) => {
  const windowWidth = 600;
  const windowHeight = 600;
  const primaryColors = ["#fac901", "#225095", "#dd0100"];
  const white = "#ffffff";
  const black = "#202020";
  // yellow, blue, red, black
  const probs = [ .2, .2, .2, .1, .3];
  const repeteProb = 0.5;
  const subdivideProb = 0.8;
  const directionProb = 0.5;
  const strokeWidth = 8;
  const minSize = 100;
  const colors = [...primaryColors, black, white];

  const getColor = () => {
    const r = Math.random();
    let sum = 0;
    for (let i=0; i<colors.length; i++){
      sum += probs[i];
      if (r <= sum) return colors[i];
    };
    return white;
  };

  const subdivide = (posX, posY, width, height, lastLine = null) => {
    if (width < minSize) return;
    if (height < minSize) return;

    const repete = (lastLine && Math.random() < repeteProb) ? true : false;
    const direction = repete ? lastLine.direction : Math.random() < directionProb ? "h" : "v";

    const spacing = repete ? lastLine.spacing*(direction === "h" ? width : height) : Math.floor(Math.random()*(direction === "h" ? height : width));
    const gap = strokeWidth/2-1;

    // rect
    const color = getColor();
    let r = Math.random();
    const rx1 = direction === 'h' ? 0 : r < 0.5 ? 0 : spacing+gap;
    const ry1 = direction === 'v' ? 0 : r < 0.5 ? 0 : spacing+gap;
    const rx2 = direction === 'h' ? width : r < 0.5 ? spacing-gap : width-spacing-gap;
    const ry2 = direction === 'v' ? height : r < 0.5 ? spacing-gap : height-spacing-gap;
    p.push();
    p.noStroke();
    p.fill(p.color(color));
    p.translate(posX, posY);
    p.rect(rx1,ry1,rx2,ry2);
    p.pop();

    // line
    const x1 = direction === 'h' ? 0 : spacing;
    const y1 = direction === 'v' ? 0 : spacing;
    const x2 = direction === 'h' ? width : spacing;
    const y2 = direction === 'v' ? height : spacing;
    p.push();
    p.stroke(p.color(black));
    p.stroke(0);
    p.strokeWeight(strokeWidth);
    p.strokeCap("square");
    p.translate(posX, posY);
    p.line(x1,y1,x2,y2);
    p.pop();

    if (Math.random() < subdivideProb) {
      subdivide(
        direction === 'h' ? 0 : spacing+gap,
        direction === 'v' ? 0 : spacing+gap,
        direction === 'h' ? width : width-gap,
        direction === 'v' ? height : height-gap,
        //{direction, spacing: spacing/(direction === "h"? height : width)}
      );
    };
    if (Math.random() < subdivideProb) {
      subdivide(
        direction === 'h' ? 0 : 0,
        direction === 'v' ? 0 : 0,
        direction === 'h' ? width : spacing-gap,
        direction === 'v' ? height : spacing-gap,
        //{direction, spacing: spacing/(direction === "h"? height : width)}
      );
    };

  };

  p.setup = function () {
    p.createCanvas(windowWidth, windowHeight);
    p.background(white);
    subdivide(0, 0, p.width, p.height);
  };
};

const sk05 = {
  title: "Mondrianesque glitch",
  description: 'Destroying a box with square subdivision.',
  canvas: s05,
};
