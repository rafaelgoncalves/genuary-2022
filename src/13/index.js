const s13 = (p) => {
  const canvasWidth = 800;
  const canvasHeight = 80;
  const squareSize = 10;

  p.setup = function () {
    p.createCanvas(canvasWidth, canvasHeight);
    p.background(0);
    p.colorMode(p.HSL);
    p.noLoop();
    p.noStroke();
  };

  p.draw = function () {
    const columns = p.width/squareSize;
    const rows = p.height/squareSize;
    for (let x = 0; x <= columns; x++) {
      for (let y = 0; y <= rows; y++) {
          let color = p.color(Math.random()*360, Math.random()*100, 50, 1);
          console.log(x, y);
          p.push();
          p.translate(x*squareSize, y*squareSize);
          p.fill(color);
          p.rect(0,0,squareSize,squareSize);
          p.pop();
      };
    };
  };
};

const sk13 = {
  title: "80x800",
  description: "Colored squares compositions on unusual dimensions.",
  canvas: s13,
};
